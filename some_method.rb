# arguments: given numbers,sample_text returns the array of valid words 
def some_method(given_numbers, sample_text) 

   words = sample_text.split(' ')
   #gets the integer digits from string
   order = words[0].split('').select { |x| x[/\d/] }

   #checks all the word if the first word contains numbers in given_numbers
   if vaildate_numbers?(order,given_numbers)
      #remove if any integer repeated consequtively
      required_order = order.select.with_index { |x,index| order[index+1]!=x }.join()
      return words.select { |word| validate_word?(required_order,word,given_numbers) }
   end
   return []
end

#check the word whether the word has sameorder of integer as in first word
def validate_word?(required_order,word,given_numbers)
   order = word.split('').select { |x| x[/\d/] }
 
   #validates the word if the word contains only the numbers in given_numbers
   if vaildate_numbers?(order,given_numbers)
      order = order.select.with_index { |x,index| order[index+1]!=x }.join()
      #returns the word which is valid to condition
      return order =~/#{required_order}.*/ ? true:false   
   end

   return false
end

#method checks the integers in a word belongs to given_numbers
def vaildate_numbers?(order,given_numbers)

   order.each do |i|
      return false if !given_numbers.include?i
   end

   return true   
end

#given_numbers = [1,5,3,7]
#sample_text = "1he3re7 is the ra1nd53om7 given t1ex3t75"

puts "enter the numbers that should be present in text"
given_numbers = gets
puts "enter your text combined with string"
sample_text=gets
output=some_method(given_numbers,sample_text) 
print output==[]? "no word matched with" : output
